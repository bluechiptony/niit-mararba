import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root"
})
export class AppService {
  public BASE_URL = "http://127.0.0.1:7000";
  constructor(private toastr: ToastrService) {}

  showErrorMessage(message) {
    this.toastr.error(message);
  }
  showWarningMessage(message) {
    this.toastr.warning(message);
  }
  showSuccessMessage(message) {
    this.toastr.success(message);
  }

  public eduLevel = [
    "SCHOOL",
    "O-LEVEL",
    "A-LEVEL",
    "COLLEGE-YEAR-1",
    "COLLEGE-YEAR-2",
    "COLLEGE-YEAR-3",
    "COLLEGE-YEAR-4",
    "POLYTECHNIC-EMPLOYED",
    "POLYTECHNIC-UNEMPLOYED",
    "IT-GRADUATE-EMPLOYED",
    "IT-GRADUATE-UNEMPLOYED",
    "NON-IT-GRADUATE-EMPLOYED",
    "NON-IT-GRADUATE-UNEMPLOYED",
    "POST-GRADUATE-EMPLOYED",
    "POST-GRADUATE-UNEMPLOYED",
    "OTHER"
  ];

  public timeSlots = ["9AM", "12PM", "2PM"];

  public sources = ["FACEBOOK", "FRIEND(S)", "FLYERS", "ONLINE", "OTHER"];
}
