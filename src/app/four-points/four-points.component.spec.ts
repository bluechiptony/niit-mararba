import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourPointsComponent } from './four-points.component';

describe('FourPointsComponent', () => {
  let component: FourPointsComponent;
  let fixture: ComponentFixture<FourPointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourPointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourPointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
