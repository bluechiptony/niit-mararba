import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AppService } from "../application/app.service";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-hero",
  templateUrl: "./hero.component.html",
  styleUrls: ["./hero.component.scss"]
})
export class HeroComponent implements OnInit {
  appForm: FormGroup;
  isSubmitted: boolean;
  timeSlots: string[] = this.app.timeSlots;
  sources = this.app.sources;
  levels = this.app.eduLevel;
  loading: boolean;
  constructor(
    private builder: FormBuilder,
    private app: AppService,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.appForm = this.builder.group({
      fullName: ["", Validators.required],
      phoneNumber: ["", Validators.required],
      emailAddress: ["", Validators.required],
      sourceOfKnowledge: [null],
      timeSlot: [null, Validators.required],
      status: [null, Validators.required]
    });
  }

  handleSubmit = (): void => {
    this.isSubmitted = true;
    if (this.appForm.valid) {
      let applicant: any = this.appForm.value;
      let name = applicant.fullName.split(" ");
      applicant.firstName = name[0];
      applicant.lastName = name[1];

      this.uploadCandidate(applicant);
    } else {
      this.app.showWarningMessage("Check your submission please");
    }
  };

  uploadCandidate = (applicant: any): void => {
    this.loading = true;
    console.log(applicant);
    let url = this.app.BASE_URL + "/applicant/create";

    this.http.post(url, applicant).subscribe(
      data => {
        console.log(data);

        this.loading = false;
        this.app.showSuccessMessage(
          "Thank you your application has been received"
        );
      },
      error => {
        console.log(error);

        this.loading = false;
        this.app.showErrorMessage(
          error.error.error
        );
      }
    );
  };

  get fullName() {
    return this.appForm.get("fullName");
  }

  get phoneNumber() {
    return this.appForm.get("fullName");
  }
  get emailAddress() {
    return this.appForm.get("emailAddress");
  }
  get sourceOfKnowledge() {
    return this.appForm.get("sourceOfKnowledge");
  }

  get timeSlot() {
    return this.appForm.get("timeSlot");
  }
  get status() {
    return this.appForm.get("status");
  }
}
